const fs = require('fs');
const path = require('path');

require('http').createServer(function(req, res){
    //Asynchronous way
    // let img = fs.readFile('./raj.jpg', (err, data) => {
    //     if(err) throw err;
    //     res.end(data);
    // });

    //using stream
     let img = fs.createReadStream("./raj.jpg", (err, data) => {
        if(err) throw err;
        data.pipe(res);
    });
}).listen(4000, function(){console.log('Server is running on 4000')});