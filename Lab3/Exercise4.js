var fs = require('fs');
var zlib = require('zlib');
var gzip = zlib.createGzip();

var rFile = fs.createReadStream('Exercise3.js');
var wFile = fs.createWriteStream('Exercise3.gz');
rFile.pipe(gzip).pipe(wFile);

setTimeout(function () {
    var gunzip = zlib.createGunzip();
    var rFile = fs.createReadStream('Exercise3.gz');
    var wFile = fs.createWriteStream('Exercise3.unzipped');
    rFile.pipe(gunzip).pipe(wFile);
},3000);
