//book
{
    "_id"       : objectId(),
    "isbn"      : isbn_number,
    "keywords"  : [],
    "auther"    : [],
    "student"   :   {
                        "studentName"   : ' ',
                        "issue_date"    : date,
                        "return_date"   : date
                    }
}


//index
db.book.createIndex({'keyword' : 1, 'studentName' : 1});
db.book.createIndex({'auther' : 1, 'studentName': 1});
db.book.createIndex({'keyword' : 1, 'return_date' : 1});
db.book.createIndex({'auther' : 1, 'return_date' : 1});
