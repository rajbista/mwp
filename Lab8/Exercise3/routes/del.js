const express = require('express');
const db = require('../dbConnection');

let app = express.Router();

app.delete('/', (req, res) => {
    var name = req.body.name;
    db.lab8.remove({'name': name});
    res.send("Data is deleted!");
});

module.exports = app;