const express = require('express');
const db = require('../dbConnection.js');
var app = express.Router();

app.post('/', function (req, res, next) {
    var name = req.body.name;
    var category = req.body.category;
    var latitude = parseFloat(req.body.latitude);
    var longitude = parseFloat(req.body.longitude);
    db.lab8.insert({ name: name, category: category, longitude: longitude, latitude: latitude }, (err, doc) => {
        if (err) throw err;
        res.send("Data is inserted!");
    });
});
module.exports = app;