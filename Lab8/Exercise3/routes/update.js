const express = require('express');
const db = require('../dbConnection');

let app = express.Router();

app.put('/', (req, res) => {
    var name = req.body.name;
    var category = req.body.category;
    var latitude = parseFloat(req.body.latitude);
    var longitude = parseFloat(req.body.longitude);

    db.lab8.update({'name': name});
    res.send("Data is deleted!");
});

module.exports = app;