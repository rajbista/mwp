const express = require('express');
const db = require('../dbConnection');

let app = express.Router();
app.get('/', (req, res) => {
    db.lab8.find().each((err, data) => {
        if (err) throw err
        res.json(data);
    });
});
module.exports = app;