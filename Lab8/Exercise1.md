1) db.restaurants.find();

2) db.restaurants.find({}, {restaurant_id:1, name:1, district:1, cuisine:1});

3) db.restaurants.find({}, {restaurant_id:1, name:1, district:1, cuisine:1, _id:0});

4) db.name.find({}, {restaurant_id:1, name:1, district:1, "address.zipcode":1});

5) db.name.find({district: "Bronx"});

6) db.name.find({district: "Bronx"}).limit(5);

7) db.name.find({district: "Bronx"}).skip(5);

8) db.name.find({"address.coord.1": {$lt  : -95.754168}});

9) db.name.find({$and: [{cuisine: {$ne : "America"}}, {grade : {$gt : 70}},{"address.coord.1": {$lt  : -95.7541;8}}]});

10) db.name.find({name: { $regex : /Wil.*/, $options: 'i'}}, {restaurant_id:1, name: 1, district:1, cuisine:1, _id:0});

11)  db.name.find({name: { $regex : /ces$/, $options: 'i'}}, {restaurant_id:1, name: 1, district:1, cuisine:1, _id:0});

12) db.name.find({name: { $regex : /reg/, $options: 'i'}}, {restaurant_id:1, name: 1, district:1, cuisine:1, _id:0});

13) db.name.find({$and:  [{district: "Bronx"}, {cuisine: {$in: ["American", "Chinese"]}}]});

14) db.name.find({district: {$in : ["Staten Island", "Queens", "Bronx", "Brooklyn"]}}, {restaurant_id:1, name:1, district:1, cuisine:1, _id:0});

15) db.name.find({district: {$nin : ["Staten Island", "Queens", "Bronx", "Brooklyn"]}}, {restaurant_id:1, name:1, district:1, cuisine:1, _id:0};

16) db.name.find({"grades.score" :{$not: {$gt: 10}}}, {restaurant_id:1, name:1, district:1, cuisine:1, _id:0}).pretty();



17) db.name.find({"location.coord.1" : {$gt: 43, $lt: 52}, {restaurant_id:1, name:1, address:1}});

18) db.name.find().sort({name:1});

19) db.name.find().sort({name:-1});

20) db.name.find().sort({{cuisine: 1}, { district: -1}});

21) db.name.find({"address.street" : {$exitst: true}});

22) db.name.find({"location.coord" : {$type: "Double"}});

23) db.name.find({name: {$regex : /^Mad.*/, $options: i}});