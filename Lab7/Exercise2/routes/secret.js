const express = require('express'),
    MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    crypto = require('crypto');

app = express.Router();


function decrypt(msg) {
    let deciper = crypto.createDecipher('aes256', 'asaadsaad')
    let val = deciper.update(msg, 'hex', 'utf8')
    val += deciper.final('utf8');
    return val;
}

//mongodb connection
MongoClient.connect('mongodb://localhost:27017/homework', (err, db) => {
    assert.equal(null, err);
    console.log('Database is connected!');
    //handaling get method
    app.get('/', function (req, res) {
        db.collection('homework7').findOne({}, (err, docs) => {
            console.log(docs.message);
            let message = decrypt(docs.message);
            res.end(message);
        });
    });
});

module.exports = app;