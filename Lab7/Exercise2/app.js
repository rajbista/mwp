const express = require('express'),
  app = express();

  const secret = require('./routes/secret.js');

  app.use('/secret', secret);

app.listen(3000);
console.log("Listening on port 3000");