  import { Component, Input } from '@angular/core';

  @Component({
    selector: 'app-my-component',
    template: `
        <ul *ngFor="let val of arr">
          <li appUpper appMyvisibility appMycolor (evtEmit)="myFun($event)">{{val}}</li>
        </ul>
    `,
    styles: []
  })
  export class MyComponentComponent {
    @Input() arr : string[];
    constructor() { }

    myFun(data){
      console.log(data);
    }
  }
