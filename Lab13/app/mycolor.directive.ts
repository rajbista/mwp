import { Directive, HostListener, ElementRef, Renderer, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appMycolor]'
})
export class MycolorDirective {
  color : string[];
  @Output() evtEmit = new EventEmitter();

  constructor( private e1 : ElementRef, private r1 : Renderer) { 
    this.color = ['red', 'yellow', 'green', 'blue'];
  }

  getRandomNumber(){
    return Math.floor(Math.random() * this.color.length);
  }

  currColor : string;

  @HostListener('click') onclick(){
    this.currColor = this.color[this.getRandomNumber()];
    this.r1.setElementStyle( this.e1.nativeElement, 'color', this.currColor );
    this.evtEmit.emit(this.currColor);
  }
}