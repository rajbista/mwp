import { Directive, ElementRef, Renderer } from '@angular/core';

@Directive({
  selector: '[appMyvisibility]'
})
export class MyvisibilityDirective {
  isVisible : boolean = true;
  constructor(private er : ElementRef, private r : Renderer) { 
    if(!this.isVisible)    r.setElementStyle(er.nativeElement, 'display', 'none')
  }

}
