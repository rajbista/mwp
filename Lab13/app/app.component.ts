import { Component } from "@angular/core";

@Component({
  selector: 'app-root',
  template: `
  <div><h1>{{pageTitle}}</h1>
    <div>My First Component</div>
  </div>
    <app-my-component [arr] = "arr1" appUpper></app-my-component>
  `
})
export class AppComponent {
  arr1 : string[]; 
  constructor(){
    this.arr1 = ['Rajan Bista', 'Mahesh Dhungana', 'Krishana Bhandari'];
  }
}
