import { Directive, ElementRef, Renderer, HostListener } from '@angular/core';

@Directive({
  selector: '[appUpper]'
})
export class UpperDirective {

  constructor(private er : ElementRef, private r : Renderer) { 
    r.setElementStyle(er.nativeElement, 'text-transform', 'uppercase');
  }

}

