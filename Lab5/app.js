const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const PORT = 8080;
const app = express();

//template engine configuration
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//other settings
app.set('case sensitive routing',true);
app.set('trust proxy',true);
app.set('case sensetive routing', true);
app.set('view cache',true);

app.use(logger('short'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', require('./routes/route_index.js'));
app.get('/users', require('./routes/route_users.js'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Page Not Found');
    err.status = 404;
    return next(new Error(err));
  });
//error handling
app.use(function (err, req, res, next) {
    res.locals.message = err.message;
    res.status(err.status || 500);
    res.render('error');
});


app.listen(PORT, () => { console.log(`Server is running on ${PORT}`) });