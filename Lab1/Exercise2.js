/* EXERCISE
1.Define a filterWords function on the String object. The Function accepts
an array of strings that specifies a list of banned words. The function returns the
string after replacing all the banned words with stars.

console.log("This house is nice!".filterWords(['house', 'nice']));
    //*This *** is***! */
    
String.prototype.filterWords = function (arr) {
    let str = this;
    for (let i = 0; i < arr.length; i++) {
        str = str.replace(arr[i], '***');
    }

    return str;
}

console.log("This house is nice!".filterWords(['house', 'nice']));