function applyCoupon(category){
    return function(dis){
        return function(item){
                function cal(){
                   return  item.price - item.price * dis;
                }
                return {
                    price : cal()
                }
        }
    }
}

var applyCoupon = category => dis => item => {
    item.price = item.price - (item.price * dis);
    return item;
}

}
const item = {
    name : "Biscuits",
    type : "regular",
    category : "food",
    price : 2.0
}
console.log(applyCoupon("food")(0.1)(item).price === 1.8);