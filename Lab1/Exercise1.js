/* EXERCISE
1. Create an object called Violinist and Pianist derived from the Musician function, which has one name property hasto be set to construct the objects.
2. Implement a method called play() that will be inherited by all objects created from Musician, the method receivesa string parameter called piece, and prints out:
    [Musician's name property] is now playing [piece] */
    
function Musician(name) {
    this.name = name;
}

Musician.prototype.play = function (piece) { console.log(this.name + ' is now playing ' + piece); }
var Violinist = new Musician("Raj Bista");
var Pianist = new Musician("Raj Bista");

Violinist.play("Soccer");