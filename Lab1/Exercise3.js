function isWeekend() {
    /*EXERCISE
        Write isWeekend function that returns the string "weekend" or "weekday" without using if-statement. */

        const todayDate = new Date();
    const day = todayDate.getDay();
    let week = {
        0: "weekend",
        1: "weekday",
        2: "weekday",
        3: "weekday",
        4: "weekday",
        5: "weekday",
        6: "weekend"
    }
    return week[day];
}
console.log(isWeekend());