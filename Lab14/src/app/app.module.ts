import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { Router } from '@angular/router/src/router';
import { Component } from '@angular/core/src/metadata/directives';
import { HomeComponent } from './home.component';
import { StudentsComponent } from './students.component';
import { DbService } from './db.service';
import { ProfileComponent } from './profile.component';
import { MyGuardService } from './my-guard.service';
import { ErrorPageComponent } from './error-page.component';

const My_Route: Routes = [
  {path : '', redirectTo: 'home', pathMatch: 'full'},
  {path : 'home', component : HomeComponent},
  {path : 'students', component: StudentsComponent},
  {path : 'profile/:id', component : ProfileComponent, canActivate: [MyGuardService]},
  {path : '**', component : ErrorPageComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StudentsComponent,
    ProfileComponent,
    ErrorPageComponent
  ],
  imports: [
    BrowserModule, RouterModule.forRoot(My_Route)
  ],
  providers: [DbService, MyGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
