import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    
    <h3> WellCome to {{title}} </h3>
  `,
  styles: []
})
export class HomeComponent implements OnInit {
  title : string = 'Maharishi University of Management';
  constructor() { }

  ngOnInit() {
  }

}
