import { Component, OnInit. OnDestroy } from '@angular/core';
import { DbService } from './db.service';
import {ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';

@Component({
  selector: 'app-profile',
  template: `
  <p> Student Details:</p>
    ID : {{students._id}}<br/>
    Name: {{students.name}} <br/>
    Email: {{students.email}} <br/>
  `,
  styles: []
})
export class ProfileComponent implements OnDestroy {
  private subscription : Subscription;
  students: any;
  id : string;
  
  constructor( private route : ActivatedRoute, private dbService : DbService) { 
     this.subscription = route.params.subscribe( (param : any) => { this.id = param['id']});
     this.students = dbService.getDataById(this.id);
  }
 
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  
}
