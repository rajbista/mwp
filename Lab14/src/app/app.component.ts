import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
<h1>  {{title}} </h1>
  
  <a [routerLink] = "['']" >Home</a> |
    <a [routerLink] = "['students']">Students</a>
    <hr>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Maharishi University Of Management';
}
