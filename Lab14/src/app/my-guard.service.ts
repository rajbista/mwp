import { Injectable } from '@angular/core';
import  {CanActivate, Router, ActivatedRouteSnapshot} from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { DbService } from './db.service';

@Injectable()
export class MyGuardService implements CanActivate{
  id : string;

  canActivate(route : ActivatedRouteSnapshot){
    if(this.dbService.isValidID(route.params['id'])){
      return true;
    }else{
      this.router.navigate(['**']);
    }
  }
  constructor(private dbService : DbService, private router : Router) { 
     
  }

}
