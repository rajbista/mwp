import { Injectable } from '@angular/core';

@Injectable()
export class DbService {
  data = [{ _id: '1', name: 'Assad Saad', stuID: '12345', email: 'assad@mum.edu' },
          {_id:'2', name: 'Raj Bista', stuID: '56789', email: 'rajbista@gmail.com'}
];
  constructor() {
  }

  getData() {
    return this.data;
  }

  getDataById(id: string) {
    let student: any;
    for (let std of this.data) {
      if (std._id == id) return std;
    }
    return null;
  }

  isValidID(id){
    for(let std of this.data){
      if(std._id == id){
        return true;
      }
    }
    return false;
  }
}
