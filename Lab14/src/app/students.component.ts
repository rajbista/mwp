import { Component, OnInit } from '@angular/core';
import { DbService } from './db.service';
import { get } from 'selenium-webdriver/http';

@Component({
  selector: 'app-students',
  template: `
    <p>List of Students:</p>
      <ul *ngFor = 'let std of students'>
        <li><a href="profile/{{std._id}}">{{std.name}}</a></li>
      </ul>
  `,
  styles: []
})
export class StudentsComponent implements OnInit {
  students : any[];

  constructor( private data : DbService) {
    this.students= data.getData();
   }


  ngOnInit() {
  }


}
