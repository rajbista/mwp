const express = require('express');
const Grade = require('../grade.js');
const { check, oneOf, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

let grade = new Grade();
let app = express.Router();

let validate = [
	check('id').exists(),
	check('name').exists(),
	check('course').exists(),
	check('grade').exists(),
  	sanitize('id').toInt()
];

app.get('/',(req,res) =>{
 res.json(grade.getGrade());
});

app.post('/', validate, function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.mapped() });
    }else {
        const data = matchedData(req);
        let rdata = grade.getGradeById(data.id);
          if(rdata != null) {
              return res.status(422).json({ errors: "Data already exist!"});
          }
          grade.add(data);
          res.status(200).json({"message": "Data is inserted!"});
      }
});
app.put('/', validate, function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.mapped() });
    }else {
        const data = matchedData(req);
        let rdata = grade.getGradeById(data.id);
          if(rdata == null) {
              return res.status(422).json({ errors: "Data is not exist!"});
          }
          grade.modify(data);
          res.status(200).json({"message": "Data is modified!"});
      }
});

app.delete('/', validate, function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.mapped() });
    }else {
        const data = matchedData(req);
        let rdata = grade.getGradeById(data.id);
          if(rdata == null) {
              return res.status(422).json({ errors: "Data is not exist!"});
          }
          grade.remove(data.id);
          res.status(200).json({"message": "Data is removed!"});
      }
});
module.exports = app;