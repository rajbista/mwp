
let grade = [];

class Grade {
    add(obj) {
        grade.push(obj);
    }

    remove(id) {
        for (let i in grade) {
            if (id == grade[i].id) {
                let temp = grade[i];
                grade.splice(i, 1);
                return temp;
            }
        }
        return null;
    }

    modify(obj) {
        for (let i in grade) {
            if (obj.id == grade[i].id) {
                let temp = grade[i];
                grade[i] = obj;
                return temp;
            }
        }
        return null;
    }
    getGrade() {
        return grade;
    }

    getGradeById(id){
        for(let i in grade){
            if(grade[i].id == id){
                return grade[i];
            }
        }
        return null;
    }
}

module.exports = Grade;