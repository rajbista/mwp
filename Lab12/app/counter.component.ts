import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-counter',
  template: `
    <p>
      <button value="minus"  (click) = "decrCounter()">-</button>
        {{counterValue}}
      <button value="minus" (click) = "incrCounter()">+</button>
    </p>
  `,
  styles: []
})
export class CounterComponent {
  counterValue : number;
  @Input() counter: number;
  @Output() counterChange = new EventEmitter();
  
  if(countr){
    this.counterValue = this.counter;
  }

  constructor() { this.counterValue = 0;}

  decrCounter(){
    this.counterValue--;
    this.counterChange.emit(this.counterValue);
  }

  incrCounter(){
    this.counterValue++;
    this.counterChange.emit(this.counterValue);
  }
}
