import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <div style="text-align:center">
  <h1>
    Welcome to {{ title }}!
  </h1>
  <div style="border: 1px, solid, blue">
    <app-counter [counterValue] = "componentCounterValue" (counterChange) = "counterChange($event)"></app-counter>
  </div>
</div>
` ,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  componentCounterValue : number;
  constructor(){ this.componentCounterValue = 5;}

  counterChange(data){
    this.componentCounterValue = parseInt(data);

    console.log(this.componentCounterValue);
  }
}
