1. Expalin why do we want sometimes to use setImmediate insted of using setTimeout?
Ans: Because setImmediate is faster than setTimeout. setImmediate doesn't use queue of functions. It checks queue of I/O event handlers, and executes the callback if all I/O events in the current snapshoot are processed. Where as setTimeout checks the timer at least once before executing the call back even it has been set to 0 millisecond delay.

2. Explain the difference between process.nextTick and setImmediate?
Ans: While callbacks queued by process.nextTice will execute before I/O and timer events, where as callbacks queued by setImmediate will be called after I/O events.

3. Name 10 global modules available in Node environment.
	i) Math
	ii) Date
	iii) Object
	iv) JSON
	v) String
	vi) Number
	vii)Error
	viii) fs
	ix) http
	x) https