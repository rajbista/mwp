1)
Ans.
        db.zips.aggregate(
    [
        {
            $match: {
                state: 'IA'
            }
        },
        {
            $group: {
                id: { state: '$state' },
                zipcodes: { $addToSet: '$_id' }
            }
        }
    ]
);

2)
Ans. 
       db.zips.aggregate([
    {
        $match: {
            pop: { $lt: 1000 }
        },
        $group: {
            _id: { state: '$state' },
            zipcodes: { $addToSet: '$_id' }
        }
    }
]);

3)
Ans.
            db.zips.aggregate(
    [
        {
            $group: {
                _id: {
                    city: '$city',
                    state: '$state'
                },
                mum_zip: {
                    $sum: 1
                }
            }
        },
        {

            $match: {
                mum_zip: {
                    $gt: 1
                }
            }
        },
        {
            $sort: {
                '_id.state': 1,
                '_id.city': 1
            }
        }
    ]
);


4)
Ans.
            db.zips.aggregate(
    [
        {
            $group: {
                _id: {
                    state: '$state',
                    city: '$city'
                },
                population: {
                    $sum: '$pop'
                }
            }
        },
        {
            $sort: {
                '_id.state': 1,
                population: 1
            }
        },
        {
            $group: {
                _id: '$_id.state',
                city: {
                    $first: '$_id.city'
                },
                population: {
                    $first: 'population'
                }
            }
        }
    ]
);